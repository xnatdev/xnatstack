# -*- mode: ruby -*-
# vi: set ft=ruby :

# XNAT Vagrant Box project
# http://www.xnat.org
# Copyright (c) 2016-2017, Washington University School of Medicine, all rights
# reserved.
# Released under the Simplified BSD license.

require 'yaml'

cwd = File.dirname(File.expand_path(__FILE__))

# load default settings
puts "Loading #{cwd}/default.yaml for Vagrant configuration..."
profile = YAML.load_file("#{cwd}/default.yaml")

# Load cached build settings.
YAML.load_file("#{cwd}/.build.yaml").each { |k, v|
  profile[k] = v
}

# load local customizations
# (We really want to read these last, but we need to read the config file from
# here.)
local_path = "#{cwd}/local.yaml"
if File.exist? local_path
  puts "Loading local overrides from #{local_path}..."
  local = YAML.load_file(local_path)
  local.each { |k, v|
    profile[k] = v
  }
end

build = profile['build']

# if for some reason an unknown build is specified, use the specified default.
unless profile['builds'][build]
  puts ''
  puts '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
  puts "Build '#{build}' has not been set in configuration files."
  build = profile['default']
  puts "Defaulting to #{build}."
  puts '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
  puts ''
end

build_config = profile['builds'][build]

box_user = build_config['box_user'] ||= ''
box_id = build_config['box_id']
box = box_user == '' ? box_id : "#{box_user}/#{box_id}"

box_url = build_config['box_url'] ||= ''
name = "xnatstack-#{box_id}"
platform = build_config['platform'] ||= 'debian'
java = build_config['java'] ||= 'openjdk-8-jdk'

script = "#{cwd}/scripts/provision_#{[build, box_id, platform].find do |key|
  File.exist? "#{cwd}/scripts/provision_#{key}.sh"
end}.sh"
unless File.exist? script.to_s
  puts 'Could not find the provisioning script for the specified platform:'
  puts "#{script}, aborting!"
  abort
end

provider = build_config['provider'] ||= 'virtualbox'

puts ''
puts "Using the #{build} profile with the following values:"
puts " * Box:           #{box}"
puts " * Box URL:       #{box_url}" if box_url != ''
puts " * Build name:    #{name}"
puts " * Platform:      #{platform}"
puts " * Provider:      #{provider}"
puts " * Provisioning:  #{script}"
puts " * Java version:  #{java}"
puts ''

VAGRANTFILE_API_VERSION = '2'.freeze

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|

  config.vm.define name
  config.vm.box = box
  config.vm.box_url = box_url if box_url != ''

  config.ssh.insert_key = false

  if Vagrant.has_plugin?("vagrant-vbguest")
    config.vbguest.auto_update = false
  end
  
  config.vm.provider provider do |v|
    v.name = name
  end

  config.vm.provision 'build', type: :shell, path: script.to_s

end
