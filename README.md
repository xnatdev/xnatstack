XNATStack Box Builder
=====================

This Vagrant project builds the XNATStack “base box”. This is different from the
standard definition of a Vagrant base box, which contains only the bare
essentials for getting a VM up and running, with all of the service
prerequisites being built on top of that for each new Vagrant instance created.
Instead the XNATStack contains the bare essentials for getting an *XNAT*
instance up and running. This includes the following tools and services:

-   [Mercurial](<https://www.mercurial-scm.org>)

-   [Git](<https://git-scm.com>)

-   [Tomcat 9](<http://tomcat.apache.org>)

-   [nginx](<https://www.nginx.com>)

-   [PostgreSQL 12](<http://www.postgresql.org>)

-   [OpenJDK 8](<openjdk.java.net>)

When building the [XNAT Vagrant
project](https://bitbucket.org/nrg/xnat_vagrant), the XNATStack box is
downloaded and cached locally. This saves a great deal of time on subsequent
builds, because the various dependencies don’t need to be downloaded, installed,
and configured for each execution.

Creating a new XNATStack box configuration
------------------------------------------

XNATStack configurations can be created in the file **default.yaml**. This file is
under source control and should only be committed back to the repo only when the box 
configuration you are adding is:

-   Intended to be a standard XNATStack box available to the entire XNAT
    development community

-   Has been built and thoroughly tested, including being used for creating an
    XNAT Vagrant build

There are five supported properties for a box build:

-   **box\_user** indicates the username for the publisher of the base box on
    which you wish to build your XNATStack box. This is optional: boxes that
    aren’t published on the central Vagrant repository often don’t have
    associated usernames.

-   **box\_id** is the ID of the box. This property is required and indicates
    the particular base box you want to use. If the **box\_user** property is
    specified, the combination of these two, in the form
    **box\_user**/**box\_id**, is used to identify the box. If **box\_user** is
    not specified, then just **box\_id** is used.

-   **box\_url** indicates the URL from which the base box can be downloaded.
    This is optional and is not necessary for boxes that are available on the
    central Vagrant repository.

-   **platform** indicates the platform or operating system for the base box.
    This affects the provisioning script that is run.

-   **provider** indicates the virtualization provider for which the base box is
    intended. This can be any valid value for the [Vagrant config.vm.provider
    property](<https://docs.vagrantup.com/v2/providers/index.html>).

These should be defined in a block corresponding to the particular box build.

Creating a new box
------------------

Once you have created a new configuration or want to build a new instance of an
existing configuration, you can build and install the box by running the provided
**build.sh** script. This will prompt you to select one of the configured boxes
and will then proceed to build the box:

-   It first builds the VM on which your box will be based by downloading the
    base box then running the platform-dependent provisioning script

-   It then turns the VM into a box file in the **boxes** folder

-   Finally it adds the box file to your local box cache
